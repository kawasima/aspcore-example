# ASP.NET coreのサンプル

## 実行

```
% dotnet watch run
```

## ロギング

PostSharpを使いInterceptorを設定する。

```csharp
        [HttpGet]
        [Route("")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Logging]
        public ActionResult<Product> Get()
        {
            return new Product
            {
                Name = "Bread"
            };
        }
```

このようにLogging Attributeを設定するだけ。

Logging Attributeは内部的にNLogを使うので、Program.csで設定をしておく

```csharp
using NLog.Web;


        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                })
            .ConfigureLogging(logging =>
            {
                logging.ClearProviders();
                logging.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Trace);
            })
            .UseNLog(); // <- NLogのセットアップ
    }
```

NLogのカスタマイズは、`NLog.config` で行う。

リクエストのトレースIDは、ASP.NET Coreに標準でTraceIDが付いているのでそれを使う。

### ToDo

ログカテゴリ(NLogではロガーのName)をどう付けるか?

- DIで渡されるLoggerはパッケージ名がLogger名になる
- Logger名はログの出力抑制に使うので、トレースログは同様にパッケージ名になるのが望ましい。
- LoggingAttributeの中で動的にLoggerを取得すれば、パッケージ名のLoggerが作れるが、性能上問題ないの? を調査の必要あり。

## Vue + RESTful API

フロントエンドのコードは、client_appにあります。
以下コマンドで開発モードで起動します。

```
% cd client_app
% npm install
% npm run serve
```

バックエンドも立ち上げれば、 `http://localhost:8080/chart` でAPIにアクセスした結果で、チャートが表示されます。


### Vueのデプロイ

環境による差異は、.env.[環境名]のファイルに切り出しておく。
.envに定義する定数名は、"VUE_APP_"のプレフィクスを付ける。

そうすることで、アプリケーションからは

```javascript
process.env.VUE_APP_BACKEND_URL
```

のようにして参照できる。

これを本番用にビルドすると、vue-cliがMinifyしたJavaScriptコード内に展開して埋め込んでくれる。
したがって、dist配下をそのままS3またはWebサーバに配置すればよい。
