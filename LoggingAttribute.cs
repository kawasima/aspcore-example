using System;
using System.Reflection;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc;
using PostSharp.Aspects;
using PostSharp.Serialization;
using PostSharp.Extensibility;
using NLog;

namespace dotnet_webapp
{
    [PSerializable]
    public sealed class LoggingAttribute : OnMethodBoundaryAspect
    {
        private static NLog.Logger logger = NLog.LogManager.GetLogger("tracing");
        private static NLog.Logger exceptionLogger = NLog.LogManager.GetLogger("exception");

        public LoggingAttribute() {
        }


        public override void OnEntry(MethodExecutionArgs e)
        {
            switch (e.Instance) {
                case PageModel m:
                    MappedDiagnosticsLogicalContext.Set("TraceId", m.HttpContext.TraceIdentifier);
                    break;
                case ControllerBase c:
                    MappedDiagnosticsLogicalContext.Set("TraceId", c.HttpContext.TraceIdentifier);
                    break;
            }
            logger.Info("Entry:{0}#{1}", e.Method.DeclaringType.Name, e.Method.Name);
        }

        public override void OnExit(MethodExecutionArgs e)
        {
            logger.Info("Exit:{0}#{1}", e.Method.DeclaringType.Name, e.Method.Name);
            switch (e.Instance) {
                case PageModel m:
                case ControllerBase c:
                    MappedDiagnosticsLogicalContext.Clear();
                    break;
            }
        }

        public override void OnException(MethodExecutionArgs e)
        {
            switch (e.Instance) {
                case PageModel m:
                case ControllerBase c:
                    exceptionLogger.Error(e.Exception);
                    break;
            }
        }
    }
}
