using System;

namespace dotnet_webapp.Models
{
    public class ApplicationRole
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}
