using System;
using System.Collections.Generic;

namespace dotnet_webapp.Models
{
    public class ResponsePage<T>
    {
        public int TotalCount { get; set; }
        public List<T> Items { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int? Next { get; set; }
        public int? Previous { get; set; }
        public int First { get; set; }
        public int Last { get; set; }
        public int Self { get; set; }

        public ResponsePage(List<T> items, int pageNumber, int pageSize) {
            this.Items = items;
            this.PageNumber = pageNumber;
            this.PageSize = pageSize;
        }
    }
}
