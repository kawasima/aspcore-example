using System;
using System.Collections.Generic;

namespace dotnet_webapp.Models
{
    public class Pokemon
    {
        public Pokemon() {
            this.Id = Guid.NewGuid();
        }

        public int No { get; set; }
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Form { get; set; }
        public bool isMegaEvolution { get; set; }
        public List<string> Types { get; set; }
        public List<string> Abilities { get; set; }
        public List<string> HiddenAbilities { get; set; }
        public PokemonStats stats { get; set; }
    }

    public class PokemonStats
    {
        public int Hp { get; set; }
        public int Attack { get; set; }
        public int Defence { get; set; }
        public int SpAttack { get; set; }
        public int SpDefence { get; set; }
        public int Speed { get; set; }
    }
}
