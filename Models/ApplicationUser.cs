using System;

namespace dotnet_webapp.Models
{
    public class ApplicationUser
    {
        public ApplicationUser()
        {
            Id = Guid.NewGuid();
        }

        public ApplicationUser(string loginName) : this()
        {
            LoginName = loginName;
        }

        public Guid Id { get; set; }

        public string LoginName { get; set; }

        public string NormalizedLoginName { get; set; }

        public string ScreenName { get; set; }

        public string Email { get; set; }

        public string PasswordHash { get; set; }

        public string UserId => Id.ToString();
    }
}
