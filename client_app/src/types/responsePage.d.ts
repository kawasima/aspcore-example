export interface ResponsePage<T> {
    self: number;
    first: number;
    last: number;
    next: number;
    previous: number;
    totalCount: number;
    items: Array<T>;
}