export interface Pokemon {
    no: number;
    id: string;
    name: string;
    form: string;
    isMegaEvolution: boolean;
    evolutions: Array<number>;
    types: Array<string>;
    abilities: Array<string>;
    hiddenAbilities: Array<string>;
    stats: {
      hp: number;
      attack: number;
      defence: number;
      spAttack: number;
      spDefence: number;
      speed: number;
    };
  }