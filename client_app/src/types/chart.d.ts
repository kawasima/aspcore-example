export interface Chart {
    plotsY: Array<Array<number>>;
    chartColor: Array<string>;
    svgWidth: number;
    svgHeight: number;
    paddingBottom: number;
    paddingLeft: number;
    paddingRight: number;
    styles: {
      backgroundColor: string;
      stroke: string;
      strokeWidth: number;
      fill: string;
    };
}