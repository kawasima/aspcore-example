import Axios, { AxiosResponse } from 'axios'
import { ResponsePage } from '@/types/responsePage';

const http = Axios.create({
    withCredentials: true,
});

http.interceptors.response.use(response => response,
    error => {
        return Promise.reject(error);
    }
);

export async function fetchPage<T>(url: URL, params: URLSearchParams | null): Promise<ResponsePage<T>> {
    if (params !== null) {
        url.search = params.toString();
    }
    const res: AxiosResponse = await http.get(url.toString());
    const responsePage: ResponsePage<T> = res.data;

    return responsePage;
}
