import { ref, watch } from "vue";

export default function(startVal: any, onValidate: (v: any) => void) {
  const input = ref(startVal);
  watch(input, value => {
    onValidate(value);
  });

  return {
    input
  }
}
