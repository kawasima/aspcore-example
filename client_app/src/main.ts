import { AxiosError } from 'axios';
import Cookies from "js-cookie";
import { ComponentPublicInstance, createApp } from 'vue'
import App from './App.vue'
import router from './router'

const errorHandler = (err: unknown, vm: ComponentPublicInstance | null, info: string) => {
    if (typeof err === "object" && err !== null) {
        const axiosError = (err as AxiosError);
        if (axiosError.response === null) {
            console.error(axiosError);
            return;
        }
        switch(axiosError.response?.status) {
            case 401:
                Cookies.remove("logined");
                router.push({ name: "Home" });                
                break;
            default:
                console.error(`Unknown Error: ${axiosError}`);
        }
    }
}
window.addEventListener("unhandledrejection", event => {
    errorHandler(event.reason, null, "");
})

const app = createApp(App);
app.config.errorHandler = errorHandler;
app.use(router).mount('#app');
