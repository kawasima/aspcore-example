using System.Linq;
using System.Collections.Generic;
using System.Text.Json;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using dotnet_webapp.Models;
using dotnet_webapp.Helpers;

namespace dotnet_webapp.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json", "text/csv")]
    public class PokemonController : ControllerBase
    {
        private readonly ILogger logger;
        private readonly List<Pokemon> pokemons;

        public PokemonController(ILogger<UserController> logger) {
            this.logger =logger;
            var jsonString = System.IO.File.ReadAllText("pokemon_data.json");
            var options = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                WriteIndented = true
            };
            pokemons = JsonSerializer.Deserialize<List<Pokemon>>(jsonString, options);
        }

        [HttpGet]
        [Route("")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Logging]
        public ActionResult<ResponsePage<Pokemon>> Get([FromQuery(Name="type")] List<string> types,
                                               [FromQuery] PaginationFilter filter) {
            if (!User.Identity.IsAuthenticated)
            {
                return Unauthorized();
            }
            logger.LogInformation(types.ToString());
            var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);

            var allItems = pokemons.FindAll(pokemon => {
                if (types.Count > 0) {
                    return types.All(t => pokemon.Types.Contains(t));
                } else {
                    return true;
                }
            });

            var items = allItems.Skip((validFilter.PageNumber - 1) * filter.PageSize)
                .Take(filter.PageSize)
                .ToList();

            return PaginationHelper.CreateResponsePage<Pokemon>(items, validFilter, allItems.Count);
        }
    }
}
