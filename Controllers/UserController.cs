using System.Threading.Tasks;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using dotnet_webapp.Models;

namespace dotnet_webapp.Controllers
{
    public class UserRequest
    {
        [Required]
        public string LoginName { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string ScreenName { get; set; }
        [Required]
        public string Password { get; set; }
    }

    public class LoginRequest
    {
        [Required]
        public string Account { get; set; }
        [Required]
        public string Password { get; set; }
    }

    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]
    public class UserController : ControllerBase
    {
        private readonly ILogger logger;
        public UserController(ILogger<UserController> logger)
        {
            this.logger =logger;
        }

        [HttpPost]
        [Route("")]
        [Logging]
        public async Task<ActionResult<ApplicationUser>> Register([FromServices]UserManager<ApplicationUser> userManager,
                                                                  [FromBody] UserRequest userReq)
        {
            var user = new ApplicationUser {
                LoginName = userReq.LoginName,
                Email = userReq.Email,
                ScreenName = userReq.ScreenName
            };
            var result = await userManager.CreateAsync(user, userReq.Password);
            logger.LogInformation(result.ToString());

            if (!result.Succeeded)
            {
                return BadRequest(new Dictionary<string, string>
                                  {
                                      ["message"] = result.ToString()
                                  });
            }
            return user;
        }

        [HttpPost]
        [Route("login")]
        [Logging]
        public async Task<IActionResult> LogIn([FromServices]SignInManager<ApplicationUser> signInManager,
                                               [FromBody] LoginRequest loginReq)
        {
            var result = await signInManager.PasswordSignInAsync(loginReq.Account, loginReq.Password, false, false);
            logger.LogInformation(result.ToString());
            if (!result.Succeeded) {
                return Unauthorized(new Dictionary<string, string>
                                    {
                                        ["message"] = result.ToString()
                                    });
            }
            return NoContent();
        }
    }
}
