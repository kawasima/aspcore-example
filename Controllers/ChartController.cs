using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using dotnet_webapp.Models;

namespace dotnet_webapp.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]
    public class ChartController : ControllerBase
    {
        [HttpGet]
        [Route("")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Logging]
        public ActionResult<List<int[]>> Get()
        {
            return new List<int[]> {
                new int[]{300, 400, 300, 400},
                new int[]{500, 200, 600, 100}
            };
        }
    }
}
