﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace dotnet_webapp.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger logger;
        public IndexModel(ILogger<IndexModel> logger)
        {
            this.logger =logger;
        }

        [LoggingAttribute]
        public void OnGet()
        {
            logger.LogInformation("OK");
            throw new System.InvalidOperationException("Logfile cannot be read-only");
        }
    }
}
