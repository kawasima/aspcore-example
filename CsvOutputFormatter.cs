namespace dotnet_webapp
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc.Formatters;
    using Microsoft.Net.Http.Headers;
    using dotnet_webapp.Models;

    public class CsvOutputFormatter : OutputFormatter
    {
        public static string Format { get; } = "csv";
        public static string ContentType { get; } = "text/csv";

        public CsvOutputFormatter()
        {
            SupportedMediaTypes.Add(MediaTypeHeaderValue.Parse(ContentType));
        }

       protected override bool CanWriteType(Type type)
        {
            if (type == null)
                throw new ArgumentNullException(nameof(type));

            return type.IsGenericType && type.GetGenericTypeDefinition() == typeof(ResponsePage<>);
        }

        private T CastObject<T>(object input) {
            return (T) input;
        }

        public override async Task WriteResponseBodyAsync(OutputFormatterWriteContext context)
        {
            var response = context.HttpContext.Response;

            Type type = context.Object.GetType();
            var itemType = type.GetGenericArguments().Length > 0 ? type.GetGenericArguments()[0] : type.GetElementType();

            var stringWriter = new StringWriter();

//            if (_options.UseSingleLineHeaderInCsv)
            if (true)
                stringWriter.WriteLine(string.Join<string>(",", itemType.GetProperties().Select(x => x.Name)));

            foreach (var obj in (IEnumerable<object>) type.GetProperty("Items").GetValue(context.Object, null))
            {
                var vals = obj.GetType().GetProperties().Select(pi => new { Value = pi.GetValue(obj, null) });

                var valueLine = string.Empty;

                foreach (var val in vals)
                {
                    if (val.Value != null)
                    {
                        var innerValue = val.Value.ToString();

                        //Check if the value contans a comma and place it in quotes if so
                        if (innerValue.Contains(","))
                            innerValue = string.Concat("\"", innerValue, "\"");

                        //Replace any \r or \n special characters from a new line with a space
                        if (innerValue.Contains("\r"))
                            innerValue = innerValue.Replace("\r", " ");
                        if (innerValue.Contains("\n"))
                            innerValue = innerValue.Replace("\n", " ");

                        valueLine = string.Concat(valueLine, innerValue, ",");
                    }
                    else
                    {
                        valueLine = string.Concat(valueLine, string.Empty, ",");
                    }
                }

                stringWriter.WriteLine(valueLine.TrimEnd(",".ToCharArray()));
            }

            var streamWriter = new StreamWriter(response.Body);
            await streamWriter.WriteAsync(stringWriter.ToString());
            await streamWriter.FlushAsync();
        }
    }
}
