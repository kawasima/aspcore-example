using System;
using System.Collections.Generic;
using dotnet_webapp.Models;

namespace dotnet_webapp.Helpers
{
    public class PaginationHelper {
        public static ResponsePage<T> CreateResponsePage<T>(List<T> items,
                                                           PaginationFilter validFilter,
                                                           int totalRecords)
        {
            var response = new ResponsePage<T>(items, validFilter.PageNumber, validFilter.PageSize);
            var totalPages = ((double)totalRecords / (double)validFilter.PageSize);
            int roundedTotalPages = Convert.ToInt32(Math.Ceiling(totalPages));
            response.Self = validFilter.PageNumber;
            response.First = 1;
            response.Last = roundedTotalPages;

            response.Next =
                validFilter.PageNumber >= 1 && validFilter.PageNumber < roundedTotalPages
                ? validFilter.PageNumber + 1 : (int?)null;
            response.Previous =
                validFilter.PageNumber - 1 >= 1 && validFilter.PageNumber <= roundedTotalPages
                ? validFilter.PageNumber - 1 : (int?)null;

            response.TotalCount = totalRecords;

            return response;
        }
    }
}
